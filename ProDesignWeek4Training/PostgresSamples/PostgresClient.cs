﻿using Npgsql;
using System;
using System.Data;

namespace ProDesignWeek4Training.PostgresSamples
{
    /* Scripts to create database and tables
    CREATE DATABASE "ProDesign.IoT"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'English_Mauritius.1252'
    LC_CTYPE = 'English_Mauritius.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

    CREATE TABLE public."Sensors"
    (
        "Name" character varying COLLATE pg_catalog."default" NOT NULL,
        "Temperature" double precision,
        "Id" integer NOT NULL,
        "Humidity" double precision,
        CONSTRAINT "Sensors_pkey" PRIMARY KEY ("Id")
    )
    */
    /// <summary>
    /// Client code to connect to postgres to retrieve/insert data.
    /// </summary>
    public class PostgresClient
    {
        /// <summary>
        /// Connection string to database
        /// </summary>
        private string m_Connectionstring = "Server=localhost;port=5432;Database=ProDesign.IoT;uid=postgres;password=password1;Application Name=My ProDesign App";
        private string m_Select_Query = "select * from \"public\".\"Sensors\";";
        private string m_Insert_Command = $"insert into \"public\".\"Sensors\"(\"Name\", \"Temperature\", \"Humidity\") values(@Name, @Temperature, @Humidity);";
        private string m_Update_Command = $"update \"public\".\"Sensors\" set \"Name\"=@Name, \"Temperature\"=@Temperature, \"Humidity\"=@Humidity where \"Id\"=@Id;";
        private string m_Delete_Command = $"delete from \"public\".\"Sensors\" where \"Id\"=@Id;";

        /// <summary>
        /// Retrieve data using DataAdapter and DataTable.
        /// This approach will open a connection, retrieve all data using provided query
        /// and close the connection.
        /// </summary>
        /// <returns></returns>
        public DataTable ExecuteQueryUsingDataAdapter()
        {
            var dataTable = new DataTable();
            //instantiate connection object using connection string
            using (NpgsqlConnection conn = new NpgsqlConnection(m_Connectionstring))
            {
                try
                {
                    //open connection to database
                    conn.Open();
                    using (var cmd = new NpgsqlCommand(m_Select_Query, conn))
                    {
                        using (var ad = new NpgsqlDataAdapter(cmd))
                        {
                            //get all data
                            ad.Fill(dataTable);
                        }
                    }
                }
                catch (Exception ne)
                {
                    //throw new Exception($"An error occurred, Error details {ne}");
                    throw;
                }
                finally
                {
                    if (conn.State == ConnectionState.Open)
                    {
                        try
                        {
                            //close connection
                            conn.Close();
                        }
                        catch { }
                    }
                }
            }
            return dataTable;
        }

        /// <summary>
        /// Retrieve data using DataReader
        /// This approach will open a connection, retrieve data one row at a time and close the connection.
        /// </summary>
        /// <returns></returns>
        public void ExecuteQueryUsingDataReader()
        {
            //instantiate connection object using connection string
            using (NpgsqlConnection conn = new NpgsqlConnection(m_Connectionstring))
            {
                try
                {
                    //open connection to database
                    conn.Open();
                    using (var cmd = new NpgsqlCommand(m_Select_Query, conn))
                    {
                        //get data one at a time
                        NpgsqlDataReader npgsqlDataReader = cmd.ExecuteReader();
                        if(npgsqlDataReader.HasRows)
                        {
                            Console.Write("Name" + "\t\t\t");
                            Console.Write("Temperature" + "\t\t\t");
                            Console.Write("Id" + "\t\t\t");
                            Console.Write("Humidity" + "\t\t\t");
                            Console.WriteLine("");
                            while (npgsqlDataReader.Read())
                            {
                                Console.Write(npgsqlDataReader.GetString(0) + "\t\t\t");
                                Console.Write(npgsqlDataReader.GetDouble(1) + "\t\t\t");
                                Console.Write(npgsqlDataReader.GetInt32(2) + "\t\t\t");
                                Console.Write(npgsqlDataReader.GetDouble(3) + "\t\t\t");
                                Console.WriteLine("");
                            }
                        }
                        
                    }
                }
                catch (NpgsqlException ne)
                {
                    //throw new Exception($"Problem running query to server, Error details {ne}");
                    throw ne;
                }
                catch (Exception e)
                {
                    //throw new Exception($"An error occurred, Error details {ne}");
                    throw;
                }
                finally
                {
                    //close connection
                    conn.Close();
                }


            }
        }

        /// <summary>
        /// Inserts a new record into the table
        /// </summary>
        /// <param name="name"></param>
        /// <param name="temperature"></param>
        /// <param name="id"></param>
        /// <param name="humidity"></param>
        public void InsertSensorData(string name, double temperature, int id, double humidity)
        {
            //instantiate connection object using connection string
            using (NpgsqlConnection conn = new NpgsqlConnection(m_Connectionstring))
            {
                try
                {
                    //open connection to database
                    conn.Open();
                    using (var cmd = new NpgsqlCommand(m_Insert_Command, conn))
                    {
                        //add parameters and execute command
                        cmd.Parameters.AddWithValue("@Name", name);
                        cmd.Parameters.AddWithValue("@Temperature", temperature);
                        //cmd.Parameters.AddWithValue("@Id", id);
                        cmd.Parameters.AddWithValue("@Humidity", humidity);
                        
                        int rowsAffected = cmd.ExecuteNonQuery();
                    }
                }
                catch (NpgsqlException ne)
                {
                    throw new Exception($"Problem running query to server, Error details {ne}");
                }
                catch (Exception ne)
                {
                    throw new Exception($"An error occurred, Error details {ne}");
                }
                finally
                {
                    //close connection
                    conn.Close();
                }
            }
        }


        /// <summary>
        /// Updates an existing record in the table
        /// </summary>
        /// <param name="name"></param>
        /// <param name="temperature"></param>
        /// <param name="id"></param>
        /// <param name="humidity"></param>
        public void UpdateSensorData(string name, double temperature, int id, double humidity)
        {
            //instantiate connection object using connection string
            using (NpgsqlConnection conn = new NpgsqlConnection(m_Connectionstring))
            {
                try
                {
                    //open connection to database
                    conn.Open();
                    using (var cmd = new NpgsqlCommand(m_Update_Command, conn))
                    {
                        //add parameters and execute command
                        cmd.Parameters.AddWithValue("@Name", name);
                        cmd.Parameters.AddWithValue("@Temperature", temperature);
                        cmd.Parameters.AddWithValue("@Id", id);
                        cmd.Parameters.AddWithValue("@Humidity", humidity);
                        int rowsAffected = cmd.ExecuteNonQuery();
                    }
                }
                catch (NpgsqlException ne)
                {
                    throw new Exception($"Problem running query to server, Error details {ne}");
                }
                catch (Exception ne)
                {
                    throw new Exception($"An error occurred, Error details {ne}");
                }
                finally
                {
                    //close connection
                    conn.Close();
                }
            }
        }
        /// <summary>
        /// Deletes a record in the table
        /// </summary>
        /// <param name="id"></param>
        public void DeleteSensorData(int id)
        {
            //instantiate connection object using connection string
            using (NpgsqlConnection conn = new NpgsqlConnection(m_Connectionstring))
            {
                try
                {
                    //open connection to database
                    conn.Open();
                    using (var cmd = new NpgsqlCommand(m_Delete_Command, conn))
                    {
                        //add parameters and execute command
                        cmd.Parameters.AddWithValue("@Id", id);
                        int rowsAffected = cmd.ExecuteNonQuery();
                    }
                }
                catch (NpgsqlException ne)
                {
                    throw new Exception($"Problem running query to server, Error details {ne}");
                }
                catch (Exception ne)
                {
                    throw new Exception($"An error occurred, Error details {ne}");
                }
                finally
                {
                    //close connection
                    conn.Close();
                }
            }
        }
    }
}
