﻿using DataTablePrettyPrinter;
using ProDesignWeek4Training.PostgresSamples;
using System;
using System.Data;
using System.Linq;

namespace ProDesignWeek4Training
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                DatabaseSamples();
                //FileSamples();

                Console.ReadKey();
            }
            catch(Exception ex)
            {
                //log.LogException("");
                var errormessage = "An error has occurred";
                Console.WriteLine(errormessage);
            }
        }

        static void DatabaseSamples()
        {
            //Initialise client to interact with database
            PostgresClient postgresClient = new PostgresClient();
            //Get data using data adapter
            DataTable dataTableSensors = postgresClient.ExecuteQueryUsingDataAdapter();
            //Display result using third party library
            if (dataTableSensors.Rows.Count > 0)
            {
                Console.WriteLine(dataTableSensors.ToPrettyPrintedString());
            }
            //get current id
            int currentId = 0;// dataTableSensors.AsEnumerable().Max(dr => dr.Field<int>("Id"));
            Random rnd = new Random();
            //insert new sample data
            postgresClient.InsertSensorData($"Sensor{currentId+1}", Math.Round(rnd.NextDouble() * 50, 2), currentId+1, Math.Round(rnd.NextDouble() * 100, 2));
            //update the sample data
            postgresClient.UpdateSensorData($"Sensor{currentId}", Math.Round(rnd.NextDouble() * 50, 2), currentId, Math.Round(rnd.NextDouble() * 100, 2));
            //Delete the sample data
            postgresClient.DeleteSensorData(currentId);

            //Get data using data reader and display it
            postgresClient.ExecuteQueryUsingDataReader();
        }

        static void FileSamples()
        {
            //Read csv file
            CsvFiles.UsingFileHelper.GetData(@"D:\Nitesh\Projects\PRODESIGN\Samples\prodesigntraining\ProDesignWeek4Training\CsvFiles\RB11E Data retrieved on 14 feb.csv", true);

            //Read csv file
            //CsvFiles.CSVFileRaw.GetData(@"D:\Nitesh\Projects\PRODESIGN\Samples\prodesigntraining\ProDesignWeek4Training\CsvFiles\RB11E Data retrieved on 14 feb.csv", true);

            //Read xlsx file
            //CsvFiles.XlsxFileRaw.GetData(@"D:\Nitesh\Projects\PRODESIGN\Samples\prodesigntraining\ProDesignWeek4Training\CsvFiles\RB11E Data retrieved on 14 feb.xlsx", true);

        }
    }
}
