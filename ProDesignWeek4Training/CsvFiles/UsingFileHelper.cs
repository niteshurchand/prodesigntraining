﻿using FileHelpers;
using System;

namespace ProDesignWeek4Training.CsvFiles
{
    [DelimitedRecord(",")]
    public class Sensors
    {
        public string SensorId;

        [FieldConverter(ConverterKind.Date, "M/d/yyyy")]
        public DateTime Date { get; set; }

        [FieldConverter(ConverterKind.Date, "H:m:s")]
        public DateTime Time;

        public string OccupyStatus;

        [FieldNullValue(0d)]
        [FieldConverter(ConverterKind.Double, ".")] // The decimal separator is .
        public double Illuminance;

        [FieldNullValue(0d)]
        [FieldConverter(ConverterKind.Double, ".")] // The decimal separator is .
        public double Temperature;

        [FieldNullValue(0d)]
        [FieldConverter(ConverterKind.Double, ".")] // The decimal separator is .
        public double BatteryVoltage;
    }


    public class UsingFileHelper
    {
        public static void GetData(string fileFullPath, bool hasHeader, int sizeToRead = 10)
        {
            var engine = new FileHelperEngine<Sensors>();
            if (hasHeader)
            {
                engine.Options.IgnoreFirstLines = 1;
            }
            var records = engine.ReadFile(fileFullPath);

            if (hasHeader)
            {
                Console.Write(engine.HeaderText.Replace(",", "\t\t"));
            }

            var count = 0;
            foreach (var record in records)
            {
                Console.Write("{0}\t\t", record.SensorId);
                Console.Write("{0}\t\t", record.Date.ToString("MM/dd/yyyy"));
                Console.Write("{0}\t\t", record.Time.ToString("HH:mm:ss"));
                Console.Write("{0}\t\t", record.OccupyStatus);
                Console.Write("{0}\t\t", record.Illuminance.ToString("#.#"));
                Console.Write("{0}\t\t", record.Temperature.ToString("#.#"));
                Console.Write("{0}\t\t", record.BatteryVoltage);
                Console.WriteLine("");
                count++;

                if (count > sizeToRead) 
                {
                    break;
                }
            }
        }
    }
}
