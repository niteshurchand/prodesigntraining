﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProDesignWeek4Training.CsvFiles
{
    class XlsxFileRaw
    {
        public static void GetData(string fileFullPath, bool hasHeader, int sizeToRead = 10)
        {
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            using (FileStream stream = File.Open(fileFullPath, FileMode.Open, FileAccess.Read))
            {
                //2. Reading from a OpenXml Excel file (2007 format; *.xlsx)
                IExcelDataReader excelReader2 = ExcelReaderFactory.CreateOpenXmlReader(stream);

                //...
                //4. DataSet - Create column names from first row
                int index = 0;
                while (excelReader2.Read())
                {
                    if (hasHeader && index == 0)
                    {
                        Console.Write("{0}\t\t", excelReader2.GetString(0));
                        Console.Write("{0}\t\t", excelReader2.GetString(1));
                        Console.Write("{0}\t\t", excelReader2.GetString(2));
                        Console.Write("{0}\t\t", excelReader2.GetString(3));
                        Console.Write("{0}\t\t", excelReader2.GetString(4));
                        Console.Write("{0}\t\t", excelReader2.GetString(5));
                        Console.Write("{0}\t\t", excelReader2.GetString(6));
                        Console.WriteLine("");
                    }
                    else
                    {
                        Console.Write("{0}\t\t", excelReader2.GetString(0));
                        Console.Write("{0}\t\t", excelReader2.GetDateTime(1).ToString("M/d/yyyy"));
                        Console.Write("{0}\t\t", excelReader2.GetDateTime(2).ToString("HH:mm:ss"));
                        Console.Write("{0}\t\t", !excelReader2.IsDBNull(3) ? excelReader2.GetDouble(3) : "");
                        Console.Write("{0}\t\t", !excelReader2.IsDBNull(4) ? excelReader2.GetDouble(4) : "");
                        Console.Write("{0}\t\t", !excelReader2.IsDBNull(5) ? excelReader2.GetDouble(5) : "");
                        Console.Write("{0}\t\t", !excelReader2.IsDBNull(6) ? excelReader2.GetDouble(6) : "");
                        Console.WriteLine("");
                    }
                    index++;
                }

                stream.Close();
            }
        }
    }
}
