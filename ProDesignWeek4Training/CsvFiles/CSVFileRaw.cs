﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProDesignWeek4Training.CsvFiles
{
    class CSVFileRaw
    {
        public static void GetData(string fileFullPath, bool hasHeader, int sizeToRead = 10)
        {
            string[] fileContents = File.ReadAllLines(fileFullPath);
            int index = 0;
            foreach(string line in fileContents)
            {
                if (index > sizeToRead)
                {
                    break;
                }
                if (line != null)
                {
                    if (hasHeader && index == 0)
                    {
                        string[] headers = line.Split(',', StringSplitOptions.RemoveEmptyEntries);
                        foreach (var s in headers)
                        {
                            Console.Write("{0}\t\t", s);
                        }
                        Console.WriteLine("");
                    }
                    else
                    {
                        string[] items = line.Split(',');
                        foreach (var s in items)
                        {
                            Console.Write("{0}\t\t", s);
                        }
                        Console.WriteLine("");
                    }
                }
                index++;
            }
        }
    }
}
