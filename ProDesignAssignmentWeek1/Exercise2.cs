﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProDesignAssignmentWeek1
{
    /*Write a sample code snippet to create an integer array structure to 
     * hold the following Pascal Triangle data

     */

    class Exercise2
    {
        public static void Test()
        {
            int[][] myJaggedArray = new int[6][];

            myJaggedArray[0] = new int[1];
            myJaggedArray[0][0] = 1;
            myJaggedArray[1] = new int[] { 1, 1 };
            myJaggedArray[2] = new int[] { 1, 2, 1 };
        }
    }
}
