﻿using System;

namespace ProDesignAssignmentWeek1
{
    class Program
    {
        static void Main(string[] args)
        {
            PassByReferenceObjectExample();
            //PassByReferenceStringExample();
            //PassByValueExample();

            //Exercise1.Test();
            //Exercise2.Test();


            ////Console.ForegroundColor = ConsoleColor.Red;
            //Console.Write("Enter your first name:");
            //string myName = Console.ReadLine();
            //Console.Write("Enter your last name:");
            //string myLastName = Console.ReadLine();
            ////Console.ForegroundColor = ConsoleColor.Blue;
            //Console.WriteLine("Hello World \"{0} {1}\"!", myName, myLastName);
            //Console.Write("Enter your score:");
            //string scoreStr = Console.ReadLine();

            ////to cater for parsing of incorrect string format
            //double score = Double.Parse(scoreStr);

            //Console.WriteLine("Your current score is {0}!", score);
            ////Console.WriteLine("Hello World!");

            Console.ReadKey();
        }


        public static void PassByValueExample()
        {
            int myAge = 20;
            Console.WriteLine("Your age is {0}!", myAge);
            IncrementMyAge(ref myAge);
            Console.WriteLine("Your age is {0}!", myAge);
        }

        public static void IncrementMyAge(ref int age)
        {
            Console.WriteLine("Your age is {0} - from called method!", age);
            age = age + 10;
            Console.WriteLine("Your age is {0} - from called method!", age);
        }

        public static void PassByReferenceStringExample()
        {
            string name = "John Doe";
            Console.WriteLine("Your name is {0}!", name);
            AddSalution(name);
            Console.WriteLine("Your name is {0}!", name);
        }
        public static void AddSalution(string name)
        {
            Console.WriteLine("Your name is {0} - from called method!", name);
            name = "Mr " + name;
            Console.WriteLine("Your name is {0} - from called method!", name);
        }


        public static void PassByReferenceObjectExample()
        {
            Person person = new Person();
            person.Name = "John Doe";
            Console.WriteLine("Your name is {0}!", person.Name);
            AddSalution(person);
            Console.WriteLine("Your name is {0}!", person.Name);
        }
        public static void AddSalution(Person person)
        {
            Console.WriteLine("Your name is {0} - from called method!", person.Name);
            person.Name = "Mr " + person.Name;
            Console.WriteLine("Your name is {0} - from called method!", person.Name);
        }
    }

    class Person
    {
        public string Name { get; set; }
    }
}
