﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProDesignAssignmentWeek1
{
    /*Write a sample code snippet to create an integer array structure 
     * to store the number of days of each calendar month for the year 2022. 
     * Example
    January has 31 days -> 1, 31
February has 28 days -> 2, 28 and so on

    */
    class Exercise1
    {
        //primitve
        //[type] [name] = [value];
        bool myBool = true;
        int myInt = 0;
        double myDouble = 0.9;
        char myChar = 'c';
        string myString = "Hello";
//Byte
//Integers
//Float, Double, Decimal
//Char
//String(Reference Type)
//DateTime


        /* Year 2022
         * January 31 days
         * Feb 28 days
         * Mar 31 days
         * 
         * 
         * 
         */
        public void Test()
        {
            //1-d array
            int[] my1DNumDaysFor2022 = { 31, 28, 31, 30 /*... }*/};
            my1DNumDaysFor2022[0] = 31;

            //2-d array
            int[,] myNumDaysFor2022 = new int[12, 2];
            myNumDaysFor2022[0, 0] = 1; //Jan
            myNumDaysFor2022[1, 0] = 2; //Feb
            myNumDaysFor2022[2, 0] = 3; //Mar


            myNumDaysFor2022[0, 1] = 31; //Jan days
            myNumDaysFor2022[1, 1] = 28; //Feb days
            myNumDaysFor2022[2, 1] = 31; //Mar days
            //continue
        }
    }
}
