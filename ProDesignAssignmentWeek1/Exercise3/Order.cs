﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProDesignAssignmentWeek1.Exercise3
{
    class Order
    {
        public Customer Customer { get; set; }
        public Product Product { get; set; }
        public double Quantity { get; set; }
        public double TotalPriceExcludingTax { get; set; }
        public double TotalPrice { get { return TotalTax + TotalPriceExcludingTax; } }
        public double TotalTax { get; set; }
        public DateTime OrderDate { get; set; }
        public string InvoiceNumber { get; set; }

        public void CalculateTax(double taxRate)
        {
            TotalTax = Product.Price * Quantity * taxRate;
        }
        public void CalculateTotalAmountExcludingTax()
        {
            TotalPriceExcludingTax = Product.Price * Quantity;
        }

    }
}
