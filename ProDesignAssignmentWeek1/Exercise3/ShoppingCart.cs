﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProDesignAssignmentWeek1.Exercise3
{
    class ShoppingCart
    {
        public Product Product { get; set; }
        public double Quantity { get; set; }
    }
}
