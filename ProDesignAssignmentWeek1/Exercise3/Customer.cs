﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProDesignAssignmentWeek1.Exercise3
{
    class Customer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ShippingAddress ShippingAddress { get; set; }
        public ShoppingCart ShoppingCart { get; set; }
        public Order[] Orders { get; set; }

        public void AddProductToCart(Product p)
        {
        }
        public void RemoveProductFromCart(Product p)
        {

        }
        public void Checkout()
        { 
        }
    }
}
