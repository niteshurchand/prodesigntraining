﻿namespace ProdesignWeek1Training
{
    /// <summary>
    /// A Sample Enumerated Data Type with default values
    /// </summary>
    enum EnumSamples
    {
        EnumDataTypeOne,
        EnumDataTypeTwo
    }

    /// <summary>
    /// A Sample Enumerated Data Type with defined values
    /// </summary>
    enum EnumWithDefinedValueSamples
    {
        EnumDataTypeOne = 1,
        EnumDataTypeTwo = 2
    }
}
