﻿namespace ProdesignWeek1Training
{
    /// <summary>
    /// Arrays in C# are zero based
    /// Note that specifying an incorrect index will raise an exception
    /// </summary>
    class ArraySamples
    {
        /// <summary>
        /// One Dimensional Array
        /// Syntax - TypeName[] VariableName = new TypeName[Size]
        /// </summary>
        public void OneDimensionalArraySample()
        {
            //declaring a 1-d array of 4 integer items
            int[] oneDimensionalArray = new int[4];
            //assigning values
            oneDimensionalArray[0] = 10;
            oneDimensionalArray[1] = 20;
            oneDimensionalArray[2] = 30;
            oneDimensionalArray[3] = 40;

            //short hand initialization if values are known during declaration
            int[] oneDimensionalArrayWithAutoInitialization = new int[4] { 10, 20, 30, 40 };
        }
        /// <summary>
        /// Two Dimensional Array
        /// Syntax - TypeName[,] VariableName = new TypeName[RowSize,ColumnSize]
        /// </summary>
        public void TwoDimensionalArraySample()
        {
            //declaring a 2-d array of 4 rows and 2 columns
            int[,] twoDimensionalArray = new int[4, 2];
            //assigning values
            twoDimensionalArray[0, 0] = 10;
            twoDimensionalArray[0, 1] = 11;
            twoDimensionalArray[1, 0] = 20;
            twoDimensionalArray[1, 1] = 21;
            twoDimensionalArray[2, 0] = 30;
            twoDimensionalArray[2, 1] = 31;
            twoDimensionalArray[3, 0] = 40;
            twoDimensionalArray[3, 1] = 41;

            //short hand initialization if values are known during declaration
            int[,] twoDimensionalArrayWithAutoInitialization = new int[4, 2] { { 10, 11 }, { 20, 21 }, { 30, 31 }, { 40, 41 } };
        }
        /// <summary>
        /// Jagged Array
        /// Syntax - TypeName[][] VariableName = new TypeName[RowSize][]
        /// </summary>
        public void JaggedArraySample()
        {
            //declaring a jagged array of 4 rows
            //assigning values
            int[][] jaggedArray = new int[4][];
            jaggedArray[0] = new int[1]; //first array with 1 item
            jaggedArray[0][0] = 10;
            jaggedArray[1] = new int[2]; //second array with 2 item
            jaggedArray[0][0] = 20;
            jaggedArray[0][1] = 21;
            jaggedArray[2] = new int[3]; //third array with 3 item
            jaggedArray[0][0] = 30;
            jaggedArray[0][1] = 31;
            jaggedArray[0][3] = 32;
            jaggedArray[3] = new int[1]; //fourth array with 1 item
            jaggedArray[3][0] = 40;

            //short hand initialization if values are known during declaration
            int[][] jaggedArrayWithAutoInitialization = new int[][] { new int[] { 10 }, new int[] { 20, 21 }, new int[] { 30, 31, 32 }, new int[] { 40, 41 } };
        }
    }
}
