﻿namespace ProdesignWeek1Training.OOPSamples
{
    /// <summary>
    /// A Screen Class which inherits from <see cref="Device"/>
    /// </summary>
    class Screen : Device
    {
        /// <summary>
        /// The screen brightness
        /// </summary>
        private int brightness;
        /// <summary>
        /// Creates a new <see cref="Screen"/>
        /// </summary>
        /// <param name="brightness">The brightness level to set</param>
        public Screen(int brightness)
        {
            this.brightness = brightness;
        }
        /// <summary>
        /// Increases the screen brightness
        /// </summary>
        public void BrightnessUp()
        {

        }
        /// <summary>
        /// Decreases the screen brightness
        /// </summary>
        public void BrightnessDown()
        {

        }
    }
}
