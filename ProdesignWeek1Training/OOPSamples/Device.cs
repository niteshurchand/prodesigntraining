﻿using System;

namespace ProdesignWeek1Training.OOPSamples
{
    /// <summary>
    /// A Device Class
    /// </summary>
    class Device
    {
        /// <summary>
        /// The device id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The device serial number
        /// </summary>
        public string SerialNumber { get; set; }
        /// <summary>
        /// The device model
        /// </summary>
        public string Model { get; set; }
        /// <summary>
        /// The device manufacture date
        /// </summary>
        public DateTime ManufacturedDate { get; set; }
        /// <summary>
        /// The device country of origin
        /// </summary>
        public string CountryOfOrigin { get; set; }
    }
}
