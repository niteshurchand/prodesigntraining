﻿namespace ProdesignWeek1Training.OOPSamples
{
    /// <summary>
    /// A Room Class
    /// </summary>
    class Room
    {
        /// <summary>
        /// The Room Name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The Room Location
        /// </summary>
        public string Location { get; set; }
    }
}
