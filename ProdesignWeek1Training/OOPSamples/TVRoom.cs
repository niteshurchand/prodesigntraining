﻿namespace ProdesignWeek1Training.OOPSamples
{
    /// <summary>
    /// A TV Room Class which inherits from <see cref="Room"/>
    /// </summary>
    class TVRoom : Room
    {
        /// <summary>
        /// The TV <see cref="TV"/>
        /// This is an aggregation relationship
        /// </summary>
        public TV TV { get; set; }
        /// <summary>
        /// The TVRemote <see cref="TVRemote"/>
        /// This is an aggregation relationship
        /// </summary>
        public TVRemote TVRemote { get; set; }
    }
}
