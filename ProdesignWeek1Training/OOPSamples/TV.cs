﻿namespace ProdesignWeek1Training.OOPSamples
{
    /// <summary>
    /// A TV Class which inherits from <see cref="Device"/>
    /// </summary>
    class TV : Device
    {
        /// <summary>
        /// The TV make
        /// </summary>
        public string Make { get; set; }

        /// <summary>
        /// The screen brightness
        /// </summary>
        public int Brightness { get; set; }
        /// <summary>
        /// Switches TV on
        /// </summary>
        public void On()
        {
            Screen = new Screen(Brightness);
        }
        /// <summary>
        /// Switches TV off
        /// </summary>
        public void Off()
        {
            Screen = null;
        }
        /// <summary>
        /// The TV Screen <see cref="Screen"/>
        /// This is a composition relationship as the screen is part of the TV
        /// </summary>
        public Screen Screen { get; set; }
        /// <summary>
        /// Increases the screen brightness
        /// </summary>
        public void BrightnessUp()
        {
            Screen.BrightnessUp();
        }
        /// <summary>
        /// Decreases the screen brightness
        /// </summary>
        public void BrightnessDown()
        {
            Screen.BrightnessDown();
        }

        /// <summary>
        /// Next channel
        /// </summary>
        public void ChannelUp()
        {
        }
        /// <summary>
        /// Previous channel
        /// </summary>
        public void ChannelDown()
        {
        }
    }
}
