﻿namespace ProdesignWeek1Training.OOPSamples
{
    /// <summary>
    /// A TV Remote which inherits from <see cref="Device"/>
    /// </summary>
    class TVRemote : Device
    {
        /// <summary>
        /// The TV <see cref="TV"/>
        /// This is an association relationship
        /// </summary>
        public TV TV { get; set; }
        /// <summary>
        /// Next channel
        /// </summary>
        public void ChannelUp()
        {
            TV.ChannelUp();
        }
        /// <summary>
        /// Previous channel
        /// </summary>
        public void ChannelDown()
        {
            TV.ChannelDown();
        }
        /// <summary>
        /// Switches TV on
        /// </summary>
        public void On()
        {
            TV.On();
        }
        /// <summary>
        /// Switches TV off
        /// </summary>
        public void Off()
        {
            TV.Off();
        }
    }
}
