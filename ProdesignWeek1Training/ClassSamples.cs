﻿namespace ProdesignWeek1Training
{
    /// <summary>
    /// A Sample Class
    /// </summary>
    class ClassSamples
    {
        /// <summary>
        /// This is a class field
        /// </summary>
        private int classField;
        /// <summary>
        /// This is a class property using a backend field <see cref="classField"/>
        /// </summary>
        public int ClassProperty { get { return classField; } set { classField = value; } }
        /// <summary>
        /// This is an auto class property i.e it does not require the developer to create 
        /// a backend field. This is taken care by the compiler.
        /// </summary>
        public int ClassAutoProperty { get; set; }
        /// <summary>
        /// This is the default constructor for <see cref="ClassSamples"/>
        /// </summary>
        public ClassSamples()
        {

        }
        /// <summary>
        /// This is another constructor for <see cref="ClassSamples"/>
        /// </summary>
        /// <param name="classField"></param>
        /// <param name="classAutoProperty"></param>
        public ClassSamples(int classField, int classAutoProperty)
        {
            this.classField = classField;
            this.ClassAutoProperty = classAutoProperty;
        }
        /// <summary>
        /// This is a class method
        /// </summary>
        /// <param name="parameterOne"></param>
        /// <param name="parameterTwo"></param>
        public void ClassMethod(int parameterOne, int parameterTwo)
        {
            //This method need to be called by an instance
        }
        /// <summary>
        /// This is a class static method
        /// </summary>
        /// <param name="parameterOne"></param>
        /// <param name="parameterTwo"></param>
        public static void StaticClassMethod(int parameterOne, int parameterTwo)
        {
            //This method can be called directly
        }
    }

    /// <summary>
    /// A Sample Static Class. It cannot have instances
    /// </summary>
    static class StaticClassSamples
    {
        /// <summary>
        /// This is an auto class property i.e it does not require the developer to create 
        /// a backend field. This is taken care by the compiler.
        /// </summary>
        public static int StaticClassAutoProperty { get; set; }
        /// <summary>
        /// This is a class static method
        /// </summary>
        /// <param name="parameterOne"></param>
        /// <param name="parameterTwo"></param>
        public static void StaticClassMethod(int parameterOne, int parameterTwo)
        {
            //This method can be called directly
        }
    }
}
