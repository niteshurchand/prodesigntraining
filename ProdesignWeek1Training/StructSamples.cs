﻿namespace ProdesignWeek1Training
{
    /// <summary>
    /// A Sample Struct - The syntax is similar to a Class with some exceptions
    /// </summary>
    struct StructSamples
    {
        /// <summary>
        /// This is a struct field
        /// </summary>
        private int structField;
        /// <summary>
        /// This is a struct property using a backend field <see cref="structField"/>
        /// </summary>
        public int StructProperty { get { return structField; } set { structField = value; } }
        /// <summary>
        /// This is an auto struct property i.e it does not require the developer to create 
        /// a backend field. This is taken care by the compiler.
        /// </summary>
        public int StructAutoProperty { get; set; }
        /// <summary>
        /// This is the constructor for <see cref="StructSamples"/>
        /// </summary>
        /// <param name="classField"></param>
        /// <param name="classAutoProperty"></param>
        public StructSamples(int classField, int classAutoProperty)
        {
            this.structField = classField;
            this.StructAutoProperty = classAutoProperty;
        }
        /// <summary>
        /// This is a struct method
        /// </summary>
        /// <param name="parameterOne"></param>
        /// <param name="parameterTwo"></param>
        public void StructMethod(int parameterOne, int parameterTwo)
        {
            //This method need to be called by an instance
        }
        /// <summary>
        /// This is a struct static method
        /// </summary>
        /// <param name="parameterOne"></param>
        /// <param name="parameterTwo"></param>
        public static void StaticStructMethod(int parameterOne, int parameterTwo)
        {
            //This method can be called directly
        }
    }
}
