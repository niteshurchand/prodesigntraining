﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProDesignAssignmentsWeek3
{
    class Exercise1
    {
        public static void Test()
        {
            Console.WriteLine("Enter the year:");
            //user input
            string yearStr = Console.ReadLine();
            //convert to year
            int year = int.Parse(yearStr);
            //determine leap year
            bool isLeapYear = false;


            //check if century
            int centuryCheck = year % 100;
            //check remainder for divisible by 4
            int remainderFor4 = year % 4;
            //check remainder for divisible by 400 (Century)
            int remainderFor400 = year % 400;
            
            //determine leap year
            if (centuryCheck == 0 && remainderFor400 == 0) //century check
            {
                isLeapYear = true;
            }
            else if (centuryCheck != 0 && remainderFor4 == 0) //normal check
            {
                isLeapYear = true;
            }
            else
            {
                isLeapYear = false;
            }

            int[] myMonthNumDays = new int[] { 31, 0, 30, 31 };

            if (isLeapYear == true)
            {
                myMonthNumDays[1] = 29;
            }
            else
            {
                myMonthNumDays[1] = 28;
            }

            Console.WriteLine("Num days for months for Year {0}", year);
            for(int i = 0; i < myMonthNumDays.Length; i++)
            {
                Console.WriteLine("Month {0} -> {1}", i + 1, myMonthNumDays[i]);
            }
        }
    }
}
