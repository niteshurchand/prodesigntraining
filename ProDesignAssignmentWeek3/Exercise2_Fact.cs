﻿using System;

namespace ProDesignAssignmentsWeek3
{
    class Exercise2_Fact
    {
        public static void Test()
        {
            Console.WriteLine("Enter the number:");
            //user input
            string numberStr = Console.ReadLine();
            //convert to number
            int number = int.Parse(numberStr);
            int factorialByLoop = FactorialByLoop(number);
            Console.WriteLine("Factorial by loop: {0}!={1}", number, factorialByLoop);
            int factorialByRecursion = FactorialByRecursion(number);
            Console.WriteLine("Factorial by recursion: {0}!={1}", number, factorialByRecursion);
            Console.ReadKey();
        }

        public static int FactorialByLoop(int number)
        {
            int product = 1;
            for(int i = number; i > 1; i--)
            {
                product = product * i;
            }
            return product;
        }
        public static int FactorialByRecursion(int number)
        {
            if (number == 1)
            {
                return 1;
            }
            int previousNumber = number - 1;
            int factorialPreviousNumber = FactorialByRecursion(previousNumber);
            int product = number * factorialPreviousNumber;
            return product;
        }
    }
}
