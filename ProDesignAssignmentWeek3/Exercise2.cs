﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProDesignAssignmentsWeek3
{
    class Exercise2
    {
        public static void Test()
        {
            Console.WriteLine("Enter the Pascal Triangle level:");
            int level = 0;
            do
            {
                //user input
                string levelStr = Console.ReadLine();
                //convert to year
                level = int.Parse(levelStr);
                if (level < 0 || level > 10)
                {
                    Console.WriteLine("Please enter a level between 1 and 10:");                    
                }
            } while (level < 0 || level > 10);


            int[][] myJaggedArray = new int[level][];

            for (int i = 0; i < level; i++)
            {
                myJaggedArray[i] = new int[i+1];
                if (i == 0)
                {
                    myJaggedArray[i][0] = 1;
                }
                else if (i == 1)
                {
                    myJaggedArray[i] = new int[] { 1, 1 };
                }
                else
                {
                    myJaggedArray[i] = new int[i + 1];
                    myJaggedArray[i][0] = 1;
                    for(var x = 1; x < i; x++)
                    {
                        myJaggedArray[i][x] = myJaggedArray[i-1][x-1] + myJaggedArray[i - 1][x];
                    }
                    myJaggedArray[i][i] = 1;
                }
            }

            for (int i = 0; i < level; i++)
            {
                for (int t = 0; t < (level - i - 1); t++)
                {
                    Console.Write("\t");
                }
                for (int c = 0; c < myJaggedArray[i].Length; c++)
                {
                    Console.Write("{0}", myJaggedArray[i][c]);
                    Console.Write("\t\t");
                }
                Console.WriteLine("");
            }
        }
    }
}
