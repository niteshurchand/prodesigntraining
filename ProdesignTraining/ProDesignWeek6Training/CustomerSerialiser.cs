﻿using Newtonsoft.Json;
using System.IO;

namespace ProDesignWeek6TrainingApi
{
    public class CustomerSerialiser
    {
        public static string Serialise(object obj)
        {
            var serializer = JsonSerializer.Create(new JsonSerializerSettings 
            {
                
            });
            using (var textWriter = new StringWriter())
            {
                serializer.Serialize(textWriter, obj);
                var json = textWriter.ToString();
                return json;
            }
        }

        public static T Deserialise<T>(string json)
        {
            var serializer = JsonSerializer.Create();
            using (var textReader = new StringReader(json))
            {
                using (var jsonReader = new JsonTextReader(textReader))
                {
                    var obj = serializer.Deserialize<T>(jsonReader);
                    return obj;
                }
            }
        }
    }
}
