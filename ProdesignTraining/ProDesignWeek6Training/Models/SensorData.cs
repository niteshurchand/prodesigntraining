﻿using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProDesignWeek6TrainingApi.Models
{
    /*
     * CREATE TABLE public."Sensors"
    (
        "Id" integer NOT NULL,
        "Name" character varying COLLATE pg_catalog."default" NOT NULL,
        "Date" date not null,
        "TemperatureC" double precision,
        CONSTRAINT "Sensors_pkey" PRIMARY KEY ("Id")
    )*/
    public class SensorData : ISensorData
    {
        private readonly string m_Connectionstring;
        private string m_Select_Query_All = "select * from \"public\".\"Sensors\";";
        private string m_Select_Query_By_Id = "select * from \"public\".\"Sensors\" where \"Id\"=@id;";
        private readonly IConfiguration m_Configuration;
        public SensorData(IConfiguration configuration)
        {
            m_Configuration = configuration;
            m_Connectionstring = configuration.GetValue<string>("ConnectionString");
        }
        public Sensor GetSensor(int id)
        {
            Sensor sensor = null;
            //instantiate connection object using connection string
            using (NpgsqlConnection conn = new NpgsqlConnection(m_Connectionstring))
            {
                try
                {
                    //open connection to database
                    conn.Open();
                    using (var cmd = new NpgsqlCommand(m_Select_Query_By_Id, conn))
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                        //get data one at a time
                        NpgsqlDataReader npgsqlDataReader = cmd.ExecuteReader();
                        if (npgsqlDataReader.HasRows)
                        {
                            npgsqlDataReader.Read();
                            sensor = new Sensor
                            {
                                Id = npgsqlDataReader.IsDBNull(0) ? default : npgsqlDataReader.GetInt32(0),
                                Name = npgsqlDataReader.IsDBNull(1) ? default : npgsqlDataReader.GetString(1),
                                Date = npgsqlDataReader.IsDBNull(2) ? default : npgsqlDataReader.GetDateTime(2),
                                TemperatureC = npgsqlDataReader.IsDBNull(3) ? default : npgsqlDataReader.GetDouble(3)
                            };
                        }
                    }
                }
                catch (NpgsqlException ne)
                {
                    throw new Exception($"Problem running query to server, Error details {ne}");
                }
                catch (Exception e)
                {
                    throw;
                }
                finally
                {
                    //close connection
                    conn.Close();
                }
            }

            return sensor;
        }

        private bool GetSensor(Sensor sensor, int id)
        {
            if (sensor.Id == id) return true;

            return false;
        }

        public IEnumerable<Sensor> GetSensors()
        {
            List<Sensor> sensors = new List<Sensor>();
            //instantiate connection object using connection string
            using (NpgsqlConnection conn = new NpgsqlConnection(m_Connectionstring))
            {
                try
                {
                    //open connection to database
                    conn.Open();
                    using (var cmd = new NpgsqlCommand(m_Select_Query_All, conn))
                    {
                        //get data one at a time
                        NpgsqlDataReader npgsqlDataReader = cmd.ExecuteReader();
                        if (npgsqlDataReader.HasRows)
                        {
                            while(npgsqlDataReader.Read())
                            {
                                sensors.Add(new Sensor 
                                {
                                    Id = npgsqlDataReader.IsDBNull(0) ? default : npgsqlDataReader.GetInt32(0),
                                    Name = npgsqlDataReader.IsDBNull(1) ? default : npgsqlDataReader.GetString(1),
                                    Date = npgsqlDataReader.IsDBNull(2) ? default : npgsqlDataReader.GetDateTime(2),
                                    TemperatureC = npgsqlDataReader.IsDBNull(3) ? default : npgsqlDataReader.GetDouble(3)                                    
                                });
                            }
                        }
                    }
                }
                catch (NpgsqlException ne)
                {
                    throw new Exception($"Problem running query to server, Error details {ne}");
                }
                catch (Exception e)
                {
                    throw;
                }
                finally
                {
                    //close connection
                    conn.Close();
                }
            }
            return sensors;
        }
    }
}
