using System;
using System.Runtime.Serialization;

namespace ProDesignWeek6TrainingApi.Models
{
    [DataContract]
    public class Sensor
    {
        [DataMember(Name = "id")]
        public int Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "date")]
        public DateTime Date { get; set; }

        [DataMember(Name = "temperatureC")]
        public double TemperatureC { get; set; }
    }
}
