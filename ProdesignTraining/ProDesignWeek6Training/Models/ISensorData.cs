﻿using System.Collections.Generic;

namespace ProDesignWeek6TrainingApi.Models
{
    public interface ISensorData
    {
        IEnumerable<Sensor> GetSensors();
        Sensor GetSensor(int id);
    }
}
