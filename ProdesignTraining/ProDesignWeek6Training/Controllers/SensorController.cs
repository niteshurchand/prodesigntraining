﻿using Microsoft.AspNetCore.Mvc;
using ProDesignWeek6TrainingApi.Models;
using System.Collections.Generic;

namespace ProDesignWeek6TrainingApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SensorController : ControllerBase
    {
        private readonly ISensorData m_SensorData;
        public SensorController(ISensorData sensorData)
        {
            m_SensorData = sensorData;
        }

        [HttpGet]
        public IEnumerable<Sensor> GetSensors()
        {
            IEnumerable<Sensor> sensors  = m_SensorData.GetSensors();
            return sensors;
        }

        [HttpGet]
        [Route("{id}")]
        public Sensor GetSensor(int id)
        {
            Sensor sensor = m_SensorData.GetSensor(id);
            return sensor;
        }
    }
}
