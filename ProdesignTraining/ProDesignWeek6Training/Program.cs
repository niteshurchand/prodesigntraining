using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ProDesignWeek6TrainingApi;
using ProDesignWeek6TrainingApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProDesignWeek6Training
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //var sensor = new Sensor
            //{
            //    Id = 1,
            //    Name = "Sensor1",
            //    Date = DateTime.Now,
            //    TemperatureC = 30
            //};

            //var json = CustomerSerialiser.Serialise(sensor);

            //var sensorFromJson = CustomerSerialiser.Deserialise<Sensor>(json);


            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
