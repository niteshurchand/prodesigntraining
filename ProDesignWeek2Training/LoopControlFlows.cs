﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProDesignWeek2Training
{
    /// <summary>
    /// Samples code for loop constructs
    /// </summary>
    class LoopControlFlows
    {
        /// <summary>
        /// Standard for loop
        /// </summary>
        public void ForLoop()
        {
            int[] myArray = new int []{ 1, 2, 3, 4, 5 };
            
            for(int i = 0; i < myArray.Length; i++)
            {
                Console.Write("{0},", myArray[i]);
            }
        }
        /// <summary>
        /// While loop - can be used alternative to for loop but need to make sure loop terminating condition is met
        /// </summary>
        public void WhileLoop()
        {
            int[] myArray = new int[] { 1, 2, 3, 4, 5 };
            
            int i = myArray.Length;
            while(i < myArray.Length) //terminating condition
            {
                Console.Write("{0},", myArray[i]);
                i++;//very important otherwise we will end up with infinite loop
            }
        }
        /// <summary>
        /// Do While loop - post condition check
        /// </summary>
        public void DoWhileLoop()
        {
            int maxRetryCount = 3;
            int attempt = 1;
            do
            {
                Console.WriteLine("Calling Api {0}", attempt);                
                attempt++;//very important otherwise we will end up with infinite loop
            } while (attempt <= maxRetryCount); //terminating condition
            /* ALternative to above but requires more checks
            int maxRetryCount = 3;
            int attempt = 0;
            while (attempt < maxRetryCount)
            {
                Console.WriteLine("Calling Api {0}", attempt + 1);
                attempt++;
            }
            */
        }
        /// <summary>
        /// Collections/Objects loop
        /// </summary>
        public void ForEachLoop()
        {
            int[] myArray = new int[] { 1, 2, 3, 4, 5 };

            foreach(int item in myArray)
            {
                Console.Write("{0},", item);
            }

        }
        /// <summary>
        /// Recursion Loop
        /// Sum all positive numbers less than initial number
        /// e.g Sum(5) = 5 + 4 + 3 + 2 + 1 = 15 
        /// </summary>
        public static int RecursionLoop(int initialNumber)
        {
            int previousNumber = initialNumber - 1;
            if (previousNumber == 0)
            {
                return 1;
            }
            int currentCount = initialNumber + RecursionLoop(previousNumber);
            return currentCount;
        }
    }    
}
