﻿using System;

namespace ProDesignWeek2Training
{
    /// <summary>
    /// Sample codes for working with Console library
    /// </summary>
    class ConsoleSamples
    {
        /// <summary>
        /// Displaying messages on the console including formatting
        /// </summary>
        public void DisplayMessages()
        {
            //Append text from current cursor position
            Console.Write("Hello World!!");
            //Append text from current cursor position and move cursor to next line
            Console.WriteLine("Hello World!!");
            //Formatting of texts
            string salutation = "Mr";
            string firstName = "John";
            string lastName = "Doe";
            //0, 1, 2 represents the parameter index
            Console.WriteLine("Hello World {0} {1} {2}!!", salutation, firstName, lastName);// Hello World Mr John Doe!!
            Console.WriteLine("Hello World {0} {2}, {1}!!", salutation, firstName, lastName);// Hello World Mr Doe, John!!
        }
        /// <summary>
        /// Reads input from the console
        /// </summary>
        public void ReadText()
        {
            //Reads text entered from current cursor position and enter key
            string line = Console.ReadLine();
        }
        /// <summary>
        /// Interacts with user
        /// </summary>
        public void InteractionsWithUser()
        {
            //Formatting of texts from user inputs interactively
            Console.WriteLine("Enter salutation [Mr, Mrs, Miss]:");
            string salutation = Console.ReadLine();
            Console.WriteLine("Enter your first name:");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter your last name:");
            string lastName = Console.ReadLine();
            Console.WriteLine("Hello World {0} {1} {2}!!", salutation, firstName, lastName);
        }
    }
}
