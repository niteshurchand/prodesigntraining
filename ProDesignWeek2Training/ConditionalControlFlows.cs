﻿using System.Collections.Generic;

namespace ProDesignWeek2Training
{
    /* Common Logical Operators
     * == equals
     * != not equals
     * > greater than
     * >= greater than or equals to
     * < less than
     * <= less than or equals to
     */

    /// <summary>
    /// Samples for conditional flows
    /// </summary>
    class ConditionalControlFlows
    {
        /// <summary>
        /// If conditions
        /// </summary>
        public void ConditionalFlows()
        {
            string responseCode = "";
            if ((responseCode == "200") || (responseCode == "201"))
            {
                //all good
            }
            else if (responseCode == "403")
            {
                //authentication required
            }
            else 
            { 
                //undefined error;
            }

            //need to log error for 403 and 401
            if ((responseCode == "401") && (responseCode == "403"))
            {
                //all good
            }

            bool resultOk = false;
            //need to log error is result is unsuccessful
            //Not operator
            if (!resultOk)
            {
                //log error
            }


            List<int> myList = new List<int>();
            if ((myList != null) && myList.Count != 0)
            {
                //this code will be executed only if object is not null and has value
            }
            //check data type of an object
            if (myList is List<int>)
            {
                //true if myList is a List<int>
            }
        }

        /// <summary>
        /// Switch statements
        /// </summary>
        public void SwitchStatmentSample()
        {
            string responseCode = "";

            switch(responseCode)
            {
                case "200":
                case "201":
                    //all good
                    break;


                case "401":
                case "403":
                    //authentication requeired

                    if (responseCode == "401")
                    {
                        //log error
                    }
                    break;

                default:
                    //undefined error;
                    break;
            }

        }
    }
    
}
